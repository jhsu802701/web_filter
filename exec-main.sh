#!/bin/bash

echo '-------------------'
echo 'sudo apt-get update'
sudo apt-get update

# Installing Python 3
echo '-----------------------------------'
echo 'sudo apt-get install -y python3-pip'
sudo apt-get install -y python3-pip

echo '------------------------------'
echo 'Backing up the /etc/hosts file'
DATE=`date +%Y_%m%d_%H%M_%S`
echo "sudo cp /etc/hosts /etc/hosts-$DATE"
sudo cp /etc/hosts /etc/hosts-$DATE

echo '--------------------------------------------------'
echo 'git clone https://github.com/StevenBlack/hosts.git'
git clone https://github.com/StevenBlack/hosts.git

DIR_WEB_FILTER=$PWD
DIR_HOSTS=$DIR_WEB_FILTER/hosts

echo '--------------------------------------------------------'
echo "cd $DIR_HOSTS && pip3 install --user -r requirements.txt"
cd $DIR_HOSTS && pip3 install --user -r requirements.txt

echo '*****************************************'
echo 'BEGIN long updates of the /etc/hosts file'
echo '*****************************************'

echo "cd $DIR_HOSTS && python3 updateHostsFile.py --auto --replace --extensions fakenews-gambling-porn-social"
cd $DIR_HOSTS && python3 updateHostsFile.py --auto --replace --extensions fakenews-gambling-porn-social

echo '********************************************'
echo 'FINISHED long updates of the /etc/hosts file'
echo '********************************************'

echo '-------------------'
echo 'Blocking news sites'
echo '' | sudo tee -a /etc/hosts
echo '#####################' | sudo tee -a /etc/hosts
echo '# Blocking news sites' | sudo tee -a /etc/hosts
echo '# Editing this file to enable these sites requires being the root/admin user' | sudo tee -a /etc/hosts

block_domain () {
  DOMAIN=$1
  echo "0.0.0.0 $DOMAIN" | sudo tee -a /etc/hosts
  echo "0.0.0.0 www.$DOMAIN" | sudo tee -a /etc/hosts
}

block_domain 'yahoo.com'
block_domain 'foxnews.com'
block_domain 'cnn.com'
block_domain 'msnbc.com'
block_domain 'freerepublic.com'
block_domain 'dailykos.com'
block_domain 'infowars.com'
block_domain 'redstate.com'
block_domain 'theblaze.com'
block_domain 'dailycaller.com'
block_domain 'usatoday.com'
block_domain 'davidwolfe.com'
block_domain 'addictinginfo.org'
block_domain 'naturalnews.com'
block_domain 'dailycaller.com.com'
block_domain 'occupydemocrats.com'
block_domain 'breitbart.com'
block_domain 'newsmax.com'
block_domain 'westernjournal.com'
block_domain 'worldtruth.tv'
block_domain 'dailywire.com'
block_domain 'nypost.com'
block_domain 'dailymail.co.uk'
block_domain 'drudgereport.com'
block_domain 'oann.com'
block_domain 'patribotics.blog'
block_domain 'palmerreport.com'
block_domain 'bipartisanreport.com'
block_domain 'alternet.org'
